import 'dart:typed_data';
import 'package:basic_utils/basic_utils.dart';
import 'package:ec/src/types.dart';
import 'package:pointycastle/key_generators/api.dart';

import 'package:ec/src/tools.dart';

final ECDomainParameters ecDomain = ECDomainParameters('prime256v1');

KeyPair generateKeyPair() {
  final keypair = (KeyGenerator('EC')
        ..init(ParametersWithRandom(
            ECKeyGeneratorParameters(ecDomain), newSecureRandom())))
      .generateKeyPair();

  return (
    publicKey:
        CryptoUtils.encodeEcPublicKeyToPem(keypair.publicKey as ECPublicKey),
    privateKey:
        CryptoUtils.encodeEcPrivateKeyToPem(keypair.privateKey as ECPrivateKey)
  );
}

Uint8List generateAesKey() => newSecureRandom().nextBytes(32);
