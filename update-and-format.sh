go get -u ./...
go mod tidy
golangci-lint run --fix
goimports -w -local gitlab.com/blauwe-knop/vorderingenoverzicht/bk-crypto .