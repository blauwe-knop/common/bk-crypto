An ECDSA and ECIES implementation in Dart.

This package currently only supports the elliptic curve known as `secp256r1` or P256, and uses AES CTR.

## Usage

### Encryption with ECIES

```dart
final message = 'Hello, world!';

final (publicKey, privateKey) = newECKeyPair();

final ciphertext = eciesEncrypt(publicKey, utf8.encode(message));
final plaintext = utf8.decode(eciesDecrypt(privateKey, ciphertext));

assert(message == plaintext);
```

### Signing with ECDSA

```dart
final message = utf8.encode('Hello, world!');

final (publicKey, privateKey) = newECKeyPair();

final signature = ecdsaSign(privateKey, message);
assert(ecdsaVerify(publicKey, signature, message));
```

### Importing and exporting keys

Existing private and public keys may also be imported and exported to/from DER and PEM using the [`basic_utils`](https://pub.dev/packages/basic_utils) package, with the following functions:
- `CryptoUtils.ecPublicKeyFromPem()`
- `CryptoUtils.ecPrivateKeyFromPem()`
- `CryptoUtils.encodeEcPublicKeyToPem()`
- `CryptoUtils.encodeEcPrivateKeyToPem()`
- `CryptoUtils.ecPublicKeyFromDerBytes()`
- `CryptoUtils.ecPrivateKeyFromDerBytes()`

This package does not seem to have functions for encoding keys to DER format.
For that, the corresponding PEM functions in conjunction with `CryptoUtils.getBytesFromPEMString()` can be used.
