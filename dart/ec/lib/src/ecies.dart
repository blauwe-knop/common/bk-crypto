import 'dart:convert';
import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart';
import 'package:ec/src/types.dart';
import 'package:pointycastle/digests/sha256.dart';

import 'package:ec/src/keys.dart';
import 'package:ec/src/tools.dart';

final int hmacSize = SHA256Digest().digestSize;
const int aesKeySize = 128 ~/ 8; // We use AES-128

// The ephemeral public key is encoded with uncompressed SEC1, which schematically is
// 0x04 + bytesOfXcoordinate + bytesOfYcoordinate.
// NB ecDomain.curve.fieldSize is in bits; we want bytes.
final int publicKeySize = ecDomain.curve.fieldSize ~/ 8 * 2 + 1;

Base64Encoded encrypt(
  PublicKeyPem publicKeyPem,
  Uint8List plaintext, {
  Uint8List? sharedInfo1,
  Uint8List? sharedInfo2,
}) {
  final ECPublicKey publicKey = CryptoUtils.ecPublicKeyFromPem(publicKeyPem);
  // Input validation
  if (!['secp256r1', 'prime256v1'].contains(publicKey.parameters?.domainName)) {
    // NB the two identifiers above both refer to the same curve
    throw Exception('unsupported curve, use secp256r1/prime256v1');
  }
  if (publicKey.Q?.isInfinity ?? true) {
    throw Exception(
        'invalid public key: Q must be set and unequal to infinity');
  }

  // Create ephemeral key pair
  final KeyPair keyPair = generateKeyPair();
  final ephemeralPrivateKey =
      CryptoUtils.ecPrivateKeyFromPem(keyPair.privateKey);
  final ephemeralPublicKey = CryptoUtils.ecPublicKeyFromPem(keyPair.publicKey);

  // Compute shared secret using ECDH, and derive keys for AES and HMAC using KDF
  final (aesKey, macKey) =
      _deriveKeys(publicKey, ephemeralPrivateKey, sharedInfo1);

  // Encrypt using AES-256 CTR
  final iv = newSecureRandom().nextBytes(aesKeySize);
  final aesParams = ParametersWithIV(KeyParameter(aesKey), iv);
  final ciphertext =
      (StreamCipher('AES/CTR')..init(true, aesParams)).process(plaintext);
  final ivAndCiphertext = Uint8List.fromList(iv + ciphertext);

  // Compute HMAC tag
  final hmacInput =
      Uint8List.fromList(ivAndCiphertext + (sharedInfo2 ?? Uint8List(0)));
  final tag =
      (Mac('SHA-256/HMAC')..init(KeyParameter(macKey))).process(hmacInput);

  // Join everything and return
  final encodedEphemeralPublicKey = ephemeralPublicKey.Q!.getEncoded(false);
  return base64Encode(
      Uint8List.fromList(encodedEphemeralPublicKey + ivAndCiphertext + tag));
}

Uint8List decrypt(
  PrivateKeyPem privateKeyPem,
  Base64Encoded cypherText, {
  Uint8List? sharedInfo1,
  Uint8List? sharedInfo2,
}) {
  final ECPrivateKey privateKey =
      CryptoUtils.ecPrivateKeyFromPem(privateKeyPem);
  // Input validation
  if (!['secp256r1', 'prime256v1']
      .contains(privateKey.parameters?.domainName)) {
    throw Exception('unsupported curve, use secp256r1/prime256v1');
  }
  if (privateKey.d == null) {
    throw Exception('invalid private key: missing d');
  }
  if (privateKey.d == BigInt.zero || privateKey.d == BigInt.one) {
    throw Exception('invalid private key: d cannot be zero or one');
  }
  final minLength = publicKeySize + aesKeySize /*iv*/ + hmacSize /*tag*/;
  final cypherTextBytes = base64Decode(cypherText);
  if (cypherTextBytes.length <= minLength) {
    throw Exception('ciphertext too short, must be more than $minLength bytes');
  }
  if (cypherTextBytes.first != 0x04) {
    // We expect the uncompressed SEC1 encoding of the ephemeral public key which always starts with 0x04.
    throw Exception('ephemeral key in unexpected format');
  }

  final (ephemeralPublicKey, iv, ciphertext, theirTag) =
      _parseInput(cypherTextBytes);

  // Compute shared secret using ECDH, and derive keys for AES and HMAC using KDF
  final (aesKey, macKey) =
      _deriveKeys(ephemeralPublicKey, privateKey, sharedInfo1);

  // Compute HMAC tag and verify it equals the one on the ciphertext
  final hmacInput =
      Uint8List.fromList(iv + ciphertext + (sharedInfo2 ?? Uint8List(0)));
  final ourTag =
      (Mac('SHA-256/HMAC')..init(KeyParameter(macKey))).process(hmacInput);
  if (!constTimeCompare(ourTag, theirTag)) {
    throw Exception('MAC validation on ciphertext failed');
  }

  // Decrypt using AES CTR
  final aesParams = ParametersWithIV(KeyParameter(aesKey), iv);
  return (StreamCipher('AES/CTR')..init(false, aesParams)).process(ciphertext);
}

(ECPublicKey, Uint8List, Uint8List, Uint8List) _parseInput(
    Uint8List encryptedData) {
  // The length of the AES ciphertext is what remains if we subtract all fixed-length parts.
  final ciphertextSize = encryptedData.length -
      publicKeySize -
      aesKeySize /*iv*/ -
      hmacSize /*tag*/;

  final parts = splitAt(
      encryptedData, [publicKeySize, aesKeySize, ciphertextSize, hmacSize]);

  final ephemeralPublicKey =
      ECPublicKey(ecDomain.curve.decodePoint(parts[0]), ecDomain);

  return (ephemeralPublicKey, parts[1], parts[2], parts[3]);
}

// Perform ECDH om the specified keys to compute a shared secret,
// and from that derive AES and HMAC keys using the Concat KDF.
(Uint8List, Uint8List) _deriveKeys(
    ECPublicKey publicKey, ECPrivateKey privateKey, Uint8List? sharedInfo1) {
  final z = (publicKey.Q! * privateKey.d)!;

  // In the version of ECIES we implement, only the x-coordinate of the Diffie-Hellman shared secret is used.
  final xBytes = z.x!.toBigInteger()!.toBytes();

  // Ensure the KDF input is always of the right size by padding 0 bytes on the left if necessary
  final xPadded =
      List<int>.filled(ecDomain.curve.fieldSize ~/ 8 - xBytes.length, 0) +
          xBytes;

  final output =
      concatKDF(Uint8List.fromList(xPadded), 2 * aesKeySize, sharedInfo1);
  return (
    output.sublist(0, aesKeySize),
    SHA256Digest().process(output.sublist(aesKeySize))
  );
}
