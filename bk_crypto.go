package bk_crypto

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/crypto/ecies"
)

func GenerateKeyPair() (*ecdsa.PrivateKey, error) {
	return ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
}

func EncodePrivateKeyToPem(privateKey *ecdsa.PrivateKey) (string, error) {
	encodedPrivateKey, err := x509.MarshalECPrivateKey(privateKey)
	if err != nil {
		return "", err
	}

	pemEncodedPrivateKey := pem.EncodeToMemory(
		&pem.Block{
			Type:  "EC PRIVATE KEY",
			Bytes: encodedPrivateKey,
		},
	)

	return string(pemEncodedPrivateKey), nil
}

func EncodePublicKeyToPem(publicKey *ecdsa.PublicKey) (string, error) {
	encodedPublicKey, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		return "", err
	}

	pemEncodedPublicKey := pem.EncodeToMemory(
		&pem.Block{
			Type:  "PUBLIC KEY",
			Bytes: encodedPublicKey,
		},
	)
	return string(pemEncodedPublicKey), nil
}

func DecodePrivateKeyFromPem(privateKeyPem string) (*ecdsa.PrivateKey, error) {
	blockPrivateKey, _ := pem.Decode([]byte(privateKeyPem))

	return x509.ParseECPrivateKey(blockPrivateKey.Bytes)
}

func DecodePublicKeyFromPem(publicKeyPem string) (*ecdsa.PublicKey, error) {
	blockPublicKey, _ := pem.Decode([]byte(publicKeyPem))

	genericPublicKey, err := x509.ParsePKIXPublicKey(blockPublicKey.Bytes)
	if err != nil {
		return nil, err
	}

	ecPublicKey, ok := genericPublicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("wrong public key type")
	}

	return ecPublicKey, nil
}

func Sign(privateKey *ecdsa.PrivateKey, message string) (string, error) {
	hash := sha256.Sum256([]byte(message))

	signature, err := ecdsa.SignASN1(rand.Reader, privateKey, hash[:])
	if err != nil {
		return "", err
	}

	base64Signature := base64.StdEncoding.EncodeToString(signature)

	return base64Signature, nil
}

func Verify(publicKey *ecdsa.PublicKey, message string, base64Signature string) (bool, error) {
	hash := sha256.Sum256([]byte(message))

	signature, err := base64.StdEncoding.DecodeString(base64Signature)
	if err != nil {
		return false, err
	}

	return ecdsa.VerifyASN1(publicKey, hash[:], []byte(signature)), nil
}

func Encrypt(publicKey *ecdsa.PublicKey, plainText []byte) ([]byte, error) {
	eciesPublicKey := ecies.ImportECDSAPublic(publicKey)
	eciesPublicKey.Curve = P256

	return ecies.Encrypt(rand.Reader, eciesPublicKey, []byte(plainText), nil, nil)
}

func Decrypt(privateKey *ecdsa.PrivateKey, ciphertext []byte) ([]byte, error) {
	eciesPrivateKey := ecies.ImportECDSA(privateKey)
	eciesPrivateKey.PublicKey.Curve = P256

	return eciesPrivateKey.Decrypt(ciphertext, nil, nil)
}

// Curve wraps elliptic.Curve from the Go standard library to implement the crypto.EllipticCurve interface
// from github.com/ethereum/go-ethereum/crypto, which is implicitly required by the Encrypt() and Decrypt()
// functions from github.com/ethereum/go-ethereum/crypto/ecies.
type Curve struct {
	elliptic.Curve
}

func (c Curve) Marshal(x, y *big.Int) []byte {
	// nolint:staticcheck
	return elliptic.Marshal(c.Curve, x, y)
}

func (c Curve) Unmarshal(data []byte) (x, y *big.Int) {
	// nolint:staticcheck
	return elliptic.Unmarshal(c.Curve, data)
}

var P256 = Curve{
	Curve: elliptic.P256(),
}

// Assert that P256 supports the interface that the Encrypt and Decrypt functions from
// github.com/ethereum/go-ethereum/crypto/ecies implicitly require.
var _ crypto.EllipticCurve = P256
