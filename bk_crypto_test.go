package bk_crypto_test

import (
	"encoding/base64"
	"testing"

	ec "gitlab.com/blauwe-knop/common/bk-crypto"

	"github.com/stretchr/testify/assert"
)

func TestECCrypto(t *testing.T) {
	//sh: openssl ecparam -name prime256v1 -genkey -out ec_private.pem
	privateKeyPem := `-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----`

	//sh: openssl ec -in ec_private.pem -pubout -out ec_public.pem
	publicKeyPem := `-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END PUBLIC KEY-----`

	t.Run("ECDSA - sign and verify with self generated key", func(t *testing.T) {
		privateKey, err := ec.GenerateKeyPair()
		assert.NoError(t, err)

		message := "A short message."

		signature, err := ec.Sign(privateKey, message)
		assert.NoError(t, err)
		assert.NotNil(t, signature)

		valid, err := ec.Verify(&privateKey.PublicKey, message, signature)
		assert.NoError(t, err)

		assert.True(t, valid)
	})

	t.Run("ECDSA - sign and verify with pregenerated key", func(t *testing.T) {
		message := "Another short message."

		privateKey, err := ec.DecodePrivateKeyFromPem(privateKeyPem)
		assert.NoError(t, err)

		signature, err := ec.Sign(privateKey, message)
		assert.NoError(t, err)
		assert.NotNil(t, signature)

		publicKey, err := ec.DecodePublicKeyFromPem(publicKeyPem)
		assert.NoError(t, err)

		valid, err := ec.Verify(publicKey, message, signature)
		assert.NoError(t, err)

		assert.True(t, valid)
	})

	t.Run("ECIES - encrypt and decrypt with self generated key", func(t *testing.T) {
		privateKey, err := ec.GenerateKeyPair()
		assert.NoError(t, err)

		plainText := "The messsage to encrypt and decrypt."

		ciphertext, err := ec.Encrypt(&privateKey.PublicKey, []byte(plainText))
		assert.NoError(t, err)
		assert.NotNil(t, ciphertext)

		decryptedText, err := ec.Decrypt(privateKey, ciphertext)
		assert.NoError(t, err)

		assert.Equal(t, plainText, string(decryptedText))
	})

	t.Run("ECIES - encrypt and decrypt with self generated key encoded and decoded pem", func(t *testing.T) {
		privateKey, err := ec.GenerateKeyPair()
		assert.NoError(t, err)

		plainText := "The messsage to encrypt and decrypt."

		publicKeyPem, err := ec.EncodePublicKeyToPem(&privateKey.PublicKey)
		assert.NoError(t, err)

		ecPublicKey, err := ec.DecodePublicKeyFromPem(publicKeyPem)
		assert.NoError(t, err)

		ciphertext, err := ec.Encrypt(ecPublicKey, []byte(plainText))
		assert.NoError(t, err)
		assert.NotNil(t, ciphertext)

		privateKeyPem, err := ec.EncodePrivateKeyToPem(privateKey)
		assert.NoError(t, err)

		ecPrivateKey, err := ec.DecodePrivateKeyFromPem(privateKeyPem)
		assert.NoError(t, err)

		decryptedText, err := ec.Decrypt(ecPrivateKey, ciphertext)
		assert.NoError(t, err)

		assert.Equal(t, plainText, string(decryptedText))
	})

	t.Run("ECIES - encrypt and decrypt with pre generated key encoded and decoded pem", func(t *testing.T) {
		plainText := `{"document":{"type":{"name":"FINANCIAL_CLAIMS_INFORMATION_REQUEST"}},"documentSignature":"MEYCIQCl3PV/DZZ2YrbRQv1n2KIelgE1+Vl6s7+c2jhmbYJGDwIhAMM2EqC1XESgty47/7jJl3RU8zLENgivu4YovQcJB6+/","certificate":"ZXlKaGJHY2lPaUpGVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhjSEJmYldGdVlXZGxjbDl2YVc0aU9pSXhNak0wTlNJc0ltRndjRjl0WVc1aFoyVnlYM0IxWW14cFkxOXJaWGtpT2lJdExTMHRMVUpGUjBsT0lGQlZRa3hKUXlCTFJWa3RMUzB0TFZ4dVRVWnJkMFYzV1VoTGIxcEplbW93UTBGUldVbExiMXBKZW1vd1JFRlJZMFJSWjBGRlprUjNjWE5tVDBocVFXMXhXRVZyU0U1cU9EZEhUemR5Y2s1Mk9GeHVPVVpoT0c5cmJqRkJObFZSYWs1R056UmljblpNVWs1NlRYRjZRbVJxVVd4UUwxaGFOMUZTV2t0VGJuRnVMemQzZWxseFUyRktlbkppZHowOVhHNHRMUzB0TFVWT1JDQlFWVUpNU1VNZ1MwVlpMUzB0TFMxY2JpSXNJbUZ3Y0Y5d2RXSnNhV05mYTJWNUlqb2lMUzB0TFMxQ1JVZEpUaUJRVlVKTVNVTWdTMFZaTFMwdExTMWNiazFHYTNkRmQxbElTMjlhU1hwcU1FTkJVVmxKUzI5YVNYcHFNRVJCVVdORVVXZEJSVXRLVmtveVlUVm5PQ3N4ZEhJd1VFOXpUR0ZzUVRGSmQybFNlRWRjYmpOck9WbGxjSEpCTjFSaFRVRmhMMkpMZEV0bGRtRm1TWGgyVEcxSlduWXdPVFJNVWtaeVR6RkdVMFZGVVZsTVNXMWlkV3AzTDBoclptYzlQVnh1TFMwdExTMUZUa1FnVUZWQ1RFbERJRXRGV1MwdExTMHRYRzRpTENKaWFYSjBhR1JoZEdVaU9pSXhPVGN3TFRBeExUQXhJaXdpWW5OdUlqb2lNVEl6TkRVMk56ZzVJaXdpWlhod0lqb3hOekUxTVRjNE1UTTBMQ0ptWVcxcGJIbGZibUZ0WlNJNklrUnZaU0lzSW1kcGRtVnVYMjVoYldVaU9pSktiMmh1SWl3aWFXRjBJam94TnpFMU1UYzNPRE0wTENKdVltWWlPakUzTVRVeE56YzNNVFI5LmI3bjF4Zy1Qd3piZ2xVelhHejQ0ZjJyWHl1Q0RqYTc5cHduWDBsMmxpU2lzWEFNNzBNOVJGNjlFbzJaMlpGR0dfWWN2OUEzYWd4UFRzTUFvek0xb2Vn","certificateType":"AppManagerJWTCertificate"}`

		ecPublicKey, err := ec.DecodePublicKeyFromPem(`-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEI2iRDd2/Dh6buDj4npvQU7XneR1t
CBZxdO2E1iwIg8i0HGLmnwwv0GvbfvjcxVJ8pt/aBETFwUDYJR8josG7ug==
-----END PUBLIC KEY-----
`)
		assert.NoError(t, err)

		ciphertext, err := ec.Encrypt(ecPublicKey, []byte(plainText))
		assert.NoError(t, err)
		assert.NotNil(t, ciphertext)

		ecPrivateKey, err := ec.DecodePrivateKeyFromPem(`-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIEmdgTOyaqJ7m/aC3IVSqDuhWybiEh2d43gprT1xW7k7oAoGCCqGSM49
AwEHoUQDQgAEI2iRDd2/Dh6buDj4npvQU7XneR1tCBZxdO2E1iwIg8i0HGLmnwwv
0GvbfvjcxVJ8pt/aBETFwUDYJR8josG7ug==
-----END EC PRIVATE KEY-----
`)
		assert.NoError(t, err)

		decryptedText, err := ec.Decrypt(ecPrivateKey, ciphertext)
		assert.NoError(t, err)

		assert.Equal(t, plainText, string(decryptedText))
	})

	t.Run("ECIES - encrypt and decrypt with pregenerated key", func(t *testing.T) {
		publicKey, err := ec.DecodePublicKeyFromPem(publicKeyPem)
		assert.NoError(t, err)

		plainText := "Another messsage to encrypt and decrypt."

		ciphertext, err := ec.Encrypt(publicKey, []byte(plainText))
		assert.NoError(t, err)
		assert.NotNil(t, ciphertext)

		privateKey, err := ec.DecodePrivateKeyFromPem(privateKeyPem)
		assert.NoError(t, err)

		decryptedText, err := ec.Decrypt(privateKey, ciphertext)
		assert.NoError(t, err)

		assert.Equal(t, plainText, string(decryptedText))
	})

	t.Run("ECIES - decrypt a ciphertext that was encrypted in Dart", func(t *testing.T) {
		privateKey, err := ec.DecodePrivateKeyFromPem(privateKeyPem)
		assert.NoError(t, err)

		ciphertext, err := base64.StdEncoding.DecodeString("BNh3mOsUUDnWu4savCNYOJ6CD28PkB8XPSRnAZcbLTPnoE7QE1h6e28LLy8DDPNRDUAyIyJQOQwQp9epoSB2Mpzur9OtKszHBIXf1ne72v+zOl86NKETfHZYP6EGxjm70CxziEejyTTDR/ckTHC9LY/pkgiAEw==")
		assert.NoError(t, err)

		plaintext, err := ec.Decrypt(privateKey, []byte(ciphertext))
		assert.NoError(t, err)

		assert.Equal(t, string(plaintext), "hello")
	})

	t.Run("ECDSA - verify a message signed in Dart", func(t *testing.T) {
		publicKey, err := ec.DecodePublicKeyFromPem(publicKeyPem)
		assert.NoError(t, err)

		signature := "MEUCIQDKqbeEuiPct+x/7qTKcxDjTL2h+gtQ0FAAqar1l0xp8QIgAs0ckfl078whHFZryGyTAdP6JLEN++qjKc9WbjbJpKE="

		result, err := ec.Verify(publicKey, "hello", signature)
		assert.NoError(t, err)
		assert.True(t, result)
	})
}
