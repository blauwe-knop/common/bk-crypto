import 'dart:typed_data';
import 'package:ec/src/tools.dart';
import "package:pointycastle/export.dart";

const int aesKeySize = 128 ~/ 8; // We use AES-128

Uint8List aesCtrEncrypt(Uint8List key, Uint8List plainText) {
  final iv = newSecureRandom().nextBytes(aesKeySize);
  final aesParams = ParametersWithIV(KeyParameter(key), iv);
  final ciphertext =
      (StreamCipher('AES/CTR')..init(true, aesParams)).process(plainText);
  final ivAndCiphertext = Uint8List.fromList(iv + ciphertext);
  return ivAndCiphertext;
}

Uint8List aesCtrDecrypt(Uint8List key, Uint8List cipherTextBytes) {
  final (iv, cipherText) = _parseInput(cipherTextBytes);
  final aesParams = ParametersWithIV(KeyParameter(key), iv);
  return (StreamCipher('AES/CTR')..init(false, aesParams)).process(cipherText);
}

(Uint8List, Uint8List) _parseInput(Uint8List encryptedData) {
  // The length of the AES ciphertext is what remains if we subtract the lenght of iv.
  final cipherTextSize = encryptedData.length - aesKeySize /*iv*/;

  final parts = splitAt(encryptedData, [aesKeySize, cipherTextSize]);

  return (parts[0], parts[1]);
}
