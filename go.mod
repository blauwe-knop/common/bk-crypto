module gitlab.com/blauwe-knop/common/bk-crypto

go 1.22.0

require (
	github.com/ethereum/go-ethereum v1.14.7
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/btcsuite/btcd/btcec/v2 v2.3.3 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.3.0 // indirect
	github.com/holiman/uint256 v1.3.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.25.0 // indirect
	golang.org/x/sys v0.23.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
