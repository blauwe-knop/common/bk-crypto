import 'dart:convert';

import 'package:ec/ec.dart';

void main() {
  final message = "Hello, world!";

  final KeyPair keyPair = generateKeyPair();

  final ciphertext = encrypt(keyPair.publicKey, utf8.encode(message));
  print(ciphertext);

  final plaintext = utf8.decode(decrypt(keyPair.privateKey, ciphertext));
  print(plaintext);

  assert(message == plaintext);
}
