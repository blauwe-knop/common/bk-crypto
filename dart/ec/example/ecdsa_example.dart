import 'dart:convert';

import 'package:ec/ec.dart';

void main() {
  final message = utf8.encode('Hello, world!');

  final KeyPair keyPair = generateKeyPair();

  final signature = sign(keyPair.privateKey, message);
  print(signature);

  assert(verify(keyPair.publicKey, signature, message));
}
