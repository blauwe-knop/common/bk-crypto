import 'dart:convert';
import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart';
import 'package:ec/src/types.dart';
import 'package:pointycastle/asn1.dart';

import 'package:ec/src/tools.dart';

Base64Encoded sign(PrivateKeyPem privateKeyPem, Uint8List message) {
  final ECPrivateKey privateKey =
      CryptoUtils.ecPrivateKeyFromPem(privateKeyPem);
  final signer = Signer('SHA-256/ECDSA')
    ..init(
        true,
        ParametersWithRandom(
            PrivateKeyParameter(privateKey), newSecureRandom()));
  final signature = signer.generateSignature(message) as ECSignature;

  return base64Encode(ASN1Sequence(
      elements: [ASN1Integer(signature.r), ASN1Integer(signature.s)]).encode());
}

bool verify(
    PublicKeyPem publicKeyPem, Base64Encoded signature, Uint8List message) {
  // Parse signature
  ECSignature ecdsaSig;
  try {
    final seq =
        ASN1Parser(base64Decode(signature)).nextObject() as ASN1Sequence;
    if (seq.elements?.length != 2) {
      throw Exception('expected two ASN1 elements');
    }
    ecdsaSig = ECSignature((seq.elements?[0] as ASN1Integer).integer!,
        (seq.elements?[1] as ASN1Integer).integer!);
  } catch (ex) {
    throw Exception('invalid signature: $ex');
  }

  // Verify signature
  final ECPublicKey publicKey = CryptoUtils.ecPublicKeyFromPem(publicKeyPem);
  final verifier = Signer('SHA-256/ECDSA')
    ..init(false, PublicKeyParameter(publicKey));
  return verifier.verifySignature(message, ecdsaSig);
}
