import 'dart:convert';
import 'dart:typed_data';
import 'package:convert/convert.dart';
import 'package:test/test.dart';
import 'package:ec/ec.dart';
import 'package:ec/src/tools.dart';

final PublicKeyPem publicKey = '''-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END PUBLIC KEY-----''';

final PrivateKeyPem privateKey = '''-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----''';

void main() {
  group('ECIES tests', () {
    test('encryption with hardcoded keys', () {
      encryptAndDecrypt(publicKey, privateKey);
    });

    test('encryption with random keys', () {
      final KeyPair keyPair = generateKeyPair();

      encryptAndDecrypt(keyPair.publicKey, keyPair.privateKey);
    });

    test('decrypt a ciphertext that was encrypted in Go', () {
      final ciphertext =
          'BMG0gADC1w3o4/Blrtr1fjgBotB4mO3eCcWT8UStlmXFQ+KicfsGGbTPz7BQcCyRE3XPT1nu7wTRMdymhJTGMgG6QSwNqmYR7jkE6uNqXSOyiGQDYdxVd0r9Z8FA2d03f8rXQUxra0aSevQJoFzUwPlUbmKQ/j4fvZwqkBxiBbxL1fgunG0/BN4ilnLhLGmMT4OCW/vgH4++';

      final plaintext = utf8.decode(decrypt(privateKey, ciphertext));
      expect(plaintext, 'Another messsage to encrypt and decrypt.');
    });

    test('decrypt a ciphertext that was encrypted in Go with shared info', () {
      final ciphertext =
          'BDmqgJucV+B549kRScymu+Ozcb8JKI7J42LIt1HXGuoLsMYUwCZ14ua3TVb2aMjNNODhjOu4VUA26NL8mx5+J8fwpFGLW2q5xvikyg1sWQRhkRNsOHCwdUnMy6gu/iKmyF6KChf+M8U9FJigiXCMp4UlQZtAOUGZIn0sDVmEd/CvbqVFOjqFL7H67mzBpL9uhO6jyLWSncqo';

      final plaintext = utf8.decode(decrypt(privateKey, ciphertext,
          sharedInfo1: Uint8List.fromList('s1'.codeUnits),
          sharedInfo2: Uint8List.fromList('s2'.codeUnits)));
      expect(plaintext, 'Another messsage to encrypt and decrypt.');
    });
  });

  group('ECDSA tests', () {
    test('sign and verify with hardcoded keys', () {
      final message = utf8.encode('hello');
      final signature = sign(privateKey, message);
      expect(verify(publicKey, signature, message), true);
    });

    test('sign and verify with random keys', () {
      final KeyPair keyPair = generateKeyPair();
      final message = utf8.encode('hello');
      final signature = sign(keyPair.privateKey, message);
      expect(verify(keyPair.publicKey, signature, message), true);
    });

    test('verify a message signed in Go', () {
      final message = utf8.encode('hello');
      final signature =
          'MEQCIGDllrmEHKzUM2HznKH9LS6cC+4MbwLOoHESp+JYymjLAiBoKTOnN/2+c6aK2/pqMb0NjWRkiCQXrgVWp7N8yW/o0g==';
      expect(verify(publicKey, signature, message), true);
    });
  });

  group('tools tests', () {
    test('ConcatKDF test', () {
      // Test case taken from https://github.com/ethereum/go-ethereum/blob/master/crypto/ecies/ecies_test.go#L51
      final output = concatKDF(Uint8List.fromList('input'.codeUnits), 32, null);
      expect(hex.encode(output),
          '858b192fa2ed4395e2bf88dd8d5770d67dc284ee539f12da8bceaa45d06ebae0');
    });

    test('constant-time list comparison', () {
      expect(
          constTimeCompare(
              Uint8List.fromList([1, 2]), Uint8List.fromList([1, 2])),
          true);

      expect(
          constTimeCompare(
              Uint8List.fromList([1, 2]), Uint8List.fromList([2, 1])),
          false);
      expect(
          constTimeCompare(
              Uint8List.fromList([1, 2]), Uint8List.fromList([1, 3])),
          false);
      expect(
          constTimeCompare(
              Uint8List.fromList([1, 2]), Uint8List.fromList([3, 4])),
          false);

      expect(
          constTimeCompare(Uint8List.fromList([1, 2]), Uint8List.fromList([1])),
          false);
      expect(
          constTimeCompare(
              Uint8List.fromList([1, 2]), Uint8List.fromList([1, 2, 3])),
          false);
    });
  });

  group('AES Key Generation, Encryption, and Decryption Tests', () {
    test('Generates a valid AES key', () {
      final key = generateAesKey();
      expect(key.length, equals(32));
    });

    test('AES CTR encryption and decryption works correctly', () async {
      // Generate a random plaintext
      final plainText = utf8.encode("Hello world");
      final key = generateAesKey();

      // Encrypt the plaintext
      final encryptedData = aesCtrEncrypt(key, plainText);

      final decryptedPlainText = aesCtrDecrypt(key, encryptedData);

      expect(decryptedPlainText, equals(plainText));
    });
  });
}

void encryptAndDecrypt(PublicKeyPem publicKey, PrivateKeyPem privateKey) {
  final message = 'hello';

  final ciphertext = encrypt(publicKey, utf8.encode(message));
  print(ciphertext);

  final decrypted = utf8.decode(decrypt(privateKey, ciphertext));
  print(decrypted);

  expect(message, decrypted);
}
