typedef PublicKeyPem = String;
typedef PrivateKeyPem = String;
typedef Base64Encoded = String;

typedef KeyPair = ({
  PublicKeyPem publicKey,
  PrivateKeyPem privateKey,
});
