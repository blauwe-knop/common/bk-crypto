import 'dart:math';
import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart';
import 'package:ec/src/types.dart';
import 'package:pointycastle/key_derivators/api.dart';
import 'package:pointycastle/random/fortuna_random.dart';

// NIST SP 800-56 Concatenation Key Derivation Function (see section 5.8.1).
Uint8List concatKDF(Uint8List input, int len, Uint8List? sharedInfo) {
  final output = Uint8List(len);
  (KeyDerivator("SHA-256/ConcatKDF")..init(HkdfParameters(input, len * 8)))
      .deriveKey(sharedInfo ?? Uint8List(0), 0, output, 0);
  return output;
}

SecureRandom newSecureRandom() {
  List<int> seed = List<int>.generate(32, (_) => Random.secure().nextInt(256));
  return FortunaRandom()..seed(KeyParameter(Uint8List.fromList(seed)));
}

bool constTimeCompare(Uint8List left, Uint8List right) {
  if (left.length != right.length) {
    return false;
  }

  var r = 0;
  for (int i = 0; i < left.length; i++) {
    r |= left[i] ^ right[i];
  }
  return r == 0;
}

// This is apparently not part of the Dart standard library.
extension ToBytes on BigInt {
  /// Convert the number to big-endian bytes.
  Uint8List toBytes() {
    if (isNegative) {
      throw Exception("can't convert negative number to bytes");
    }

    // >> 3 effictively divides by 8, and adding 7 rounds up instead of down
    int bytes = (bitLength + 7) >> 3;

    final b256 = BigInt.from(256);
    final result = Uint8List(bytes);
    var number = this;
    for (int i = 0; i < bytes; i++) {
      result[bytes - 1 - i] = number.remainder(b256).toInt();
      number = number >> 8;
    }

    return result;
  }
}

// Split a byte sequence in parts of the specified lengths.
List<Uint8List> splitAt(Uint8List input, List<int> lengths) {
  List<Uint8List> output = List.empty(growable: true);
  var rest = input;

  for (var length in lengths) {
    output.add(rest.sublist(0, length));
    rest = rest.sublist(length);
  }

  return output;
}

void verifyEcPublicKey(PublicKeyPem publicKeyPem) {
  final ECPublicKey publicKey = CryptoUtils.ecPublicKeyFromPem(publicKeyPem);

  if (!['secp256r1', 'prime256v1'].contains(publicKey.parameters?.domainName)) {
    // NB the two identifiers above both refer to the same curve
    throw Exception('unsupported curve, use secp256r1/prime256v1');
  }
  if (publicKey.Q?.isInfinity ?? true) {
    throw Exception(
        'invalid public key: Q must be set and unequal to infinity');
  }
}

void verifyEcPrivateKey(PrivateKeyPem privateKeyPem) {
  final ECPrivateKey privateKey =
      CryptoUtils.ecPrivateKeyFromPem(privateKeyPem);
  // Input validation
  if (!['secp256r1', 'prime256v1']
      .contains(privateKey.parameters?.domainName)) {
    throw Exception('unsupported curve, use secp256r1/prime256v1');
  }
  if (privateKey.d == null) {
    throw Exception('invalid private key: missing d');
  }
  if (privateKey.d == BigInt.zero || privateKey.d == BigInt.one) {
    throw Exception('invalid private key: d cannot be zero or one');
  }
}
